<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Admin group routes
Route::group(['prefix'=>'customer/dashboard'], function () {

	Route::get('tasks', function () {
	    return view('home');
	});

	Route::get('tasks', array(
			'as' => 'customer.all_tasks',
			'uses' => 'TaskController@index'
	));

	Route::get('create-task', array(
			'as' => 'customer.create_task',
			'uses' => 'TaskController@create'
	));

	Route::post('store-task', array(
			'as' => 'customer.store_task',
			'uses' => 'TaskController@store'
	));

	Route::get('task/{id}', array(
			'as' => 'customer.task.details',
			'uses' => 'TaskController@show'
	));

	Route::get('update/{id}', array(
			'as' => 'customer.task.edit',
			'uses' => 'TaskController@edit'
	));

	Route::put('update/{id}', array(
			'as' => 'customer.task.update',
			'uses' => 'TaskController@update'
	));

	Route::delete('delete/{id}', array(
			'as' => 'customer.task.delete',
			'uses' => 'TaskController@destroy'
	));
});	

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
