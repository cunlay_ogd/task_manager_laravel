<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'cat_id', 'user_id'
    ];

    public function getTaskCategoryName()
    {
    	return $this->taskCategory->name;
    }

    public function taskCategory()
    {
        return $this->belongsTo('App\TaskCategory','cat_id');
    }

    public function users()
    {
        return $this->belongsTo('App\Users','user_id');
    }
}
