<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Task;
use App\TaskCategory;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check() AND Auth::user()->hasRole('ADMIN')) {
            $tasks = Task::all();
        } elseif (Auth::check() AND Auth::user()->hasRole('CUSTOMER')) {
            $tasks = Task::where('user_id', Auth::user()->id)
                       ->orderBy('id', 'desc')
                       ->paginate(10);
        } else {
            Auth::logout();
            return redirect()->route('/');
        }

        return view('tasks.tasks')->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $task_categories= TaskCategory::pluck('name','id');

        return view('tasks.create',compact('task_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:256',
            'description' => 'required|string|min:10',
            'cat_id' => 'required|integer',
        ]);

        $task = new Task; 
        $task->name = $request->name; 
        $task->description = $request->description;
        $task->cat_id = $request->category; 
        $task->user_id = Auth::user()->id; 

        if ($task->save() == true) {
            return redirect()->route('customer.all_tasks');
        }
        else {
            return back()->with('error_message', 'Task could not be created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);

        if ($task == FALSE) {
            return redirect()->route('customer.all_tasks')->with('error_message','Category not found');
        }

        return view('tasks.task_details')->with('task', $task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['task'] = Task::findorfail($id);
        $db_task_category= TaskCategory::all();

        $new_task_category = [];
        foreach ($db_task_category as $category) {
            $new_task_category[$category->id] = $category->name;
        }

        $data['task_categories']= (object)$new_task_category;

        // print_r($data); return;

        return view('tasks.update')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findorfail($id);
        $task->update($request->all());
        return redirect()->route('customer.all_tasks')->with('success_message','Task was successfully updated'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        if ($task) { 
            $task->delete();
            return redirect()->route('customer.all_tasks')->with('success_message','Task Deleted'); 
        }
        return back()->with('error_message','Task was not found, please try again');
    }
}
