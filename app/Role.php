<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public static $rules = array('name'=>'required|min:3|unique:roles,name');

    public function users()
    {
    	return $this->hasMany('App\User');
    }
}
