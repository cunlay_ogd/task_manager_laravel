<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'phone', 'address', 'country_id', 'account_status', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole($roleName)
    {
        $role_details = $this->roles;
        // dd($role_details); return;
        // echo $role_details->name;
        if ($role_details->name == $roleName) {
            return true;
        }
        return false;
    }

    public function roles()
    {
         // dd($this->belongsTo('App\Role','role_id'));
        return $this->belongsTo('App\Role','role_id');
    }

}
