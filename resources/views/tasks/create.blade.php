@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Create Task</h4></div>

                <div class="panel-body">
                    {!! Form::open(['route' => 'customer.store_task']) !!}

                    <div class="form-group col-md-12 {{ $errors->has('name') ? ' has-error' : '' }}">
                        {{ Form::label('name', 'Task Name', ['class' => 'control-label']) }}
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Task Name', 'required' => '']) }}
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-md-12 {{ $errors->has('category') ? ' has-error' : '' }}">
                        {{ Form::label('cat_id', 'Task Category', ['class' => 'control-label']) }}
                        {{ Form::select('cat_id', $task_categories,null,['class' => 'form-control', 'required' => '']) }}
                        
                        @if ($errors->has('category'))
                            <span class="help-block">
                                <strong>{{ $errors->first('category') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-md-12 {{ $errors->has('description') ? ' has-error' : '' }}">
                        {{ Form::label('description', 'Task Description', ['class' => 'control-label']) }}
                        {{ Form::textarea('description', null, ['class' => 'form-control','placeholder' => 'Task Description', 'required' => '']) }}
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        {{ Form::submit('Add Task',['class' => 'form-control btn btn-primary']) }}
                    </div>

                {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
