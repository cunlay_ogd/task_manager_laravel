@extends('layouts.app')
@section('title', 'Task Details')
@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h4 class="page-header">Title: {{ $task->name }}</h4>
		    <div class="row">
				<div class="col-md-3">Task ID:</div>
				<div class="col-md-4">{{ $task->id }}</div>
			</div>
			<div class="row">
				<div class="col-md-3">Task Category:</div>
				<div class="col-md-4">{{ $task->getTaskCategoryName() }}</div>
			</div>
			<div class="row">
				<div class="col-md-3">Task description:</div>
				<div class="col-md-4">{{ $task->description }}</div>
			</div>
			<div class="row">
				<div class="col-md-3">Created_at:</div>
				<div class="col-md-4">{{ $task->created_at }}</div>
			</div>
			<div class="row">
				<div class="col-md-3">Updated_at:</div>
				<div class="col-md-4">{{ $task->updated_at }}</div>
			</div>
		</div>
	</div>
@endsection