@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h4 class="page-header">My tasks</h4>
            @forelse ($tasks as $task)
                <div class="row" style="margin-bottom: 10px; border-bottom: 1px solid black; padding-bottom: 10px">
                    <div class="col-md-6">{{ $task->name }}</div>
                    <div class="col-md-2"><a href="{{ route('customer.task.details', ['id' => $task->id]) }}" class="btn btn-info">Show details</a></div>
                    <div class="col-md-2"><a href="{{ route('customer.task.edit', ['id' => $task->id]) }}" class="btn btn-warning">Edit details</a></div>
                    {!! Form::open(['method'=>'delete', 'route' => ['customer.task.delete', $task->id]]) !!}
                        <div class=" col-md-2">
                            {{ Form::submit('Delete',['class' => 'btn btn-danger']) }}
                        </div>
                    {!! Form::close() !!}
                </div>
            @empty
                <p>No Task to display</p>
            @endforelse
            {{ $tasks->links() }}
        </div>
    </div>
</div>
@endsection
